import Map from "./components/Map";

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(<Map />, document.querySelector("#react-map-root"));
});
