class PopupInfo extends React.PureComponent {
  render() {
    const { info } = this.props;

    return (
      <div>
        <p>{info.name}</p>
        <p>
          <img width={50} src={"//placehold.it/100x100"} />
        </p>
      </div>
    );
  }
}

export default PopupInfo;
