import "isomorphic-fetch";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import Pin from "./Pin";
import PopupInfo from "./PopupInfo";

class Map extends React.Component {
  constructor(props) {
    super(props);
    const {
      latitude,
      longitude,
      zoom,
      endpoint,
      token,
      style
    } = window.__INITIAL_DATA__;
    this.state = {
      viewport: {
        width: "100%",
        height: 600,
        latitude: +latitude,
        longitude: +longitude,
        zoom: +zoom
      },
      token,
      style,
      endpoint,
      popupInfo: null,
      markers: []
    };
  }

  _updateViewport = viewport => {
    this.setState({ viewport });
  };

  _renderMarker = (data, i) => {
    if (data.contact.latitude && data.contact.longitude) {
      return (
        <Marker
          key={`marker-${i}`}
          longitude={+data.contact.longitude}
          latitude={+data.contact.latitude}
        >
          <Pin
            size={20}
            onClick={() => this.setState({ popupInfo: data.contact })}
          />
        </Marker>
      );
    }
  };

  _renderPopup() {
    const { popupInfo } = this.state;

    return (
      popupInfo && (
        <Popup
          tipSize={5}
          anchor="top"
          longitude={+popupInfo.longitude}
          latitude={+popupInfo.latitude}
          closeOnClick={false}
          onClose={() => this.setState({ popupInfo: null })}
        >
          <PopupInfo info={popupInfo} />
        </Popup>
      )
    );
  }

  componentDidMount() {
    delete window.__INITIAL_DATA__;
    fetch(this.state.endpoint)
      .then(res => res.json())
      .then(json => {
        this.setState({ markers: json });
      });
  }

  render() {
    console.log(this.state);
    return (
      <ReactMapGL
        {...this.state.viewport}
        onViewportChange={viewport => this.setState({ viewport })}
        mapStyle={`mapbox://styles/mapbox/${this.state.style}`}
        mapboxApiAccessToken={this.state.token}
      >
        {this.state.markers.map(this._renderMarker)}
        {this._renderPopup()}
      </ReactMapGL>
    );
  }
}

export default Map;
