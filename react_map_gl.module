<?php

function react_map_gl_block_info() {
  $blocks['react_map_gl'] = [
    'info' => t('MapboxGL'),
    'cache' => DRUPAL_NO_CACHE
  ];
  return $blocks;
}

function react_map_gl_block_configure($delta = '') {
  $form = [];
  switch($delta) {
    case 'react_map_gl':
      $form = [
        'defaults' => [
          '#type' => 'fieldset',
          '#title' => t('Map Defaults'),
          'latitude' => [
            '#type' => 'textfield',
            '#title' => t('Latitude'),
            '#default_value' => variable_get('react_map_gl_lat', ''),
            '#description' => t('Default latitude when map initially renders ')
          ],
          'longitude' => [
            '#type' => 'textfield',
            '#title' => t('Longitude'),
            '#default_value' => variable_get('react_map_gl_lng', ''),
            '#description' => t('Default longitude when map initially renders')
          ],
          'zoom' => [
            '#type' => 'textfield',
            '#title' => t('Zoom'),
            '#default_value' => variable_get('react_map_gl_zoom', '8'),
            '#description' => t('Default zoom distance when map initially renders')
          ]
        ],
        'data' => [
          '#type' => 'fieldset',
          '#title' => t('Data'),
          'endpoint' => [
            '#type' => 'textfield',
            '#title' => t('Endpoint'),
            '#default_value' => variable_get('react_map_gl_endpoint', ''),
            '#description' => t('JSON formatted array of markers')
          ],
          'root_object_name' => [
            '#type' => 'textfield',
            '#title' => t('Root object name'),
            '#default_value' => variable_get('react_map_gl_root_object_name', '')
          ],
          'child_root_object_name' => [
            '#type' => 'textfield',
            '#title' => t('Child root object name'),
            '#default_value' => variable_get('react_map_gl_child_root_object_name', '')
          ]
        ],
        'mapbox' => [
          '#type' => 'fieldset',
          '#title' => t('Mapbox Fields'),
          'mapbox_token' => [
            '#type' => 'textfield',
            '#title' => t('Mapbox Access token'),
            '#default_value' => variable_get('react_map_gl_token', '')
          ],
          'mapbox_style' => [
            '#type' => 'select',
            '#title' => t('Map Style'),
            '#options' => array(
              'streets-v11' => 'streets-v11',
              'light-v10' => 'light-v10',
              'dark-v10' => 'dark-v10',
              'outdoors-v11' => 'outdoors-v11',
              'satellite-v9' => 'satellite-v9'
            ),
            '#default_value' => variable_get('react_map_gl_style', 'streets-v11')
          ]
        ],
        'mapbox_markers' => [
          '#type' => 'fieldset',
          '#title' => t('Marker Properties'),
          'mapbox_marker_size' => [
            '#type' => 'textfield',
            '#title' => t('Height'),
            '#default_value' => variable_get('react_map_gl_marker_size', 20)
          ],
          'mapbox_marker_fill' => [
            '#type' => 'textfield',
            '#title' => t('Fill color'),
            '#default_value' => variable_get('react_map_gl_marker_fill', 20)
          ],
          'mapbox_marker_stroke' => [
            '#type' => 'textfield',
            '#title' => t('Stroke color'),
            '#default_value' => variable_get('react_map_gl_marker_stroke', 20)
          ]
        ],
        'react' => [
          '#type' => 'fieldset',
          '#title' => t('ReactJS'),
          'minify' => [
            '#type' => 'checkbox',
            '#title' => t('Minify ReactJS'),
            '#default_value' => variable_get('react_map_gl_minify', 1),
            '#description' => t('It is recommended that you minify ReactJS on production websites')
          ]
        ]
      ];
      break;
  }
  return $form;
}

function react_map_gl_block_save($delta = '', $edit = []) {
  switch($delta) {
    case 'react_map_gl':
      variable_set('react_map_gl_lat', $edit['latitude']);
      variable_set('react_map_gl_lng', $edit['longitude']);
      variable_set('react_map_gl_zoom', $edit['zoom']);
      variable_set('react_map_gl_endpoint', $edit['endpoint']);
      variable_set('react_map_gl_root_object_name', $edit['root_object_name']);
      variable_set('react_map_gl_child_root_object_name', $edit['child_root_object_name']);
      variable_set('react_map_gl_token', $edit['mapbox_token']);
      variable_set('react_map_gl_style', $edit['mapbox_style']);
      variable_set('react_map_gl_minify', $edit['minify']);
      break;
  }
}

function react_map_gl_block_view($delta = '') {
  $block = [];
  switch($delta) {
    case 'react_map_gl':
      $block['content'] = react_map_gl_view();
      break;
  }
  return $block;
}

function react_map_gl_view() {
  $block = [];
  $minify = variable_get('react_map_gl_minify', 1);
  $lat = variable_get('react_map_gl_lat', '37.0902');
  $lng = variable_get('react_map_gl_lng', '-95.7129');
  $zoom = variable_get('react_map_gl_zoom', '1');
  $endpoint = variable_get('react_map_gl_endpoint', '');
  $token = variable_get('react_map_gl_token', '');
  $style = variable_get('react_map_gl_style', 'streets-v11');

  if($minify) {
    $react_path = '/' . drupal_get_path('module', 'react_map_gl') . '/node_modules/react/umd/react.production.min.js';
    $react_dom_path = '/' . drupal_get_path('module', 'react_map_gl') . '/node_modules/react-dom/umd/react-dom.production.min.js';
    $root_component = '/' . drupal_get_path('module', 'react_map_gl') . '/dist/react-map-gl.bundle.js';
  }
  else {
    $react_path = '/' . drupal_get_path('module', 'react_map_gl') . '/node_modules/react/umd/react.development.js';
    $react_dom_path = '/' . drupal_get_path('module', 'react_map_gl') . '/node_modules/react-dom/umd/react-dom.development.js';
    $root_component = '/' . drupal_get_path('module', 'react_map_gl') . '/dist/react-map-gl.development.js';
  }
  $mapbox_stylesheet = '/' . drupal_get_path('module', 'react_map_gl') . '/node_modules/mapbox-gl/dist/mapbox-gl.css';

  $initial_data = [
    'latitude' => $lat,
    'longitude' => $lng,
    'zoom' => $zoom,
    'endpoint' => $endpoint,
    'token' => $token,
    'style' => $style
  ];

  drupal_add_css($mapbox_stylesheet);
  drupal_add_js($react_path);
  drupal_add_js($react_dom_path);
  drupal_add_js($root_component);
  drupal_add_js('window.__INITIAL_DATA__ = ' . json_encode($initial_data), 'inline');
  
  $block = [
    'endpoint' => [
      '#type' => 'markup',
      '#markup' => '<div id="react-map-root"></div>'
    ]
  ];
  return $block;
}

?>