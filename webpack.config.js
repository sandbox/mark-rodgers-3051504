const path = require("path");

const baseConfig = {
  target: "web",
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};

if (process.env.NODE_ENV == "development") {
  var config = {
    ...baseConfig,
    mode: "development",
    output: {
      path: path.join(__dirname, "dist"),
      filename: "react-map-gl.development.js"
    }
  };
} else if (process.env.NODE_ENV == "production") {
  var config = {
    ...baseConfig,
    mode: "production",
    output: {
      path: path.join(__dirname, "dist"),
      filename: "react-map-gl.bundle.js"
    }
  };
}

module.exports = config;
